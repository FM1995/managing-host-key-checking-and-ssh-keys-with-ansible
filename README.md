# Managing Host Key Checking and SSH keys with Ansible

#### Project Outline

If we want to automate the configuration of the servers using ansible without the interactive key check, we can configure authorized keys and known hosts. This will be suitable for permanent servers as oppose to ephemeral servers

#### Lets get started

Lets begin by creating a new droplet

![Image1](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image1.png)

So we need to ensure the ansible machine is able to connect to the droplet and the Dropler server needs to recognise it is a valid host

So we need to add the droplet server details in the known hosts

![Image2](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image2.png)

Can execute the below command

```
ssh-keyscan -H 46.101.73.22 > ~/.ssh/known_hosts
```

![Image3](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image3.png)

And can see the known host files got updated

![Image4](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image4.png)

Can then ssh into it without the prompt

![Image5](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image5.png)

Can also create another droplet using password instead of ssh key

![Image6](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image6.png)

Can do the same for the below

```
ssh-keyscan -H 159.65.202.198 > ~/.ssh/known_hosts
```

![Image7](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image7.png)

Can see no need to verify

![Image8](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image8.png)

Since the authorized keys is empty, what we can do is copy the key into the authroized keys file

Can copy the key from the default location which id_rsa.pub t our intended host

Using the below command

```
ssh-copy-id root@159.65.202.198
```

![Image9](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image9.png)

Now we can authorize and ssh in, as the public key is now in the authorized keys section

And can see in the below the password is not required when we try to ssh in

![Image10](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image10.png)

And then checking the authorized keys

```
cat .ssh/authorized_keys
```

![Image11](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image11.png)

Can then add the new droplet IP’s to the host

![Image12](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image12.png)

Can then utilize the ansible command to connect to the droplets

```
ansible droplet -I hosts -m ping
```

![Image13](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image13.png)

Now lets disable host key checking

Lets say we have server which are dynamically created and destroyed

Let’s destroy the last two created droplets and create a new droplet

![Image14](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image14.png)

Can then create the below as the ansible file on newer versions is not available

![Image15](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image15.png)

Can then configure the host, where it allows so that the when the ssh connection occurs it does not request for a prompt

![Image16](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image16.png)

And the new IP

![Image17](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image17.png)

Can then run it again using the ansible command

```
ansible droplet -I hosts -m ping
```

And can see success and did not get any prompt

![Image18](https://gitlab.com/FM1995/managing-host-key-checking-and-ssh-keys-with-ansible/-/raw/main/Images/Image18.png)



